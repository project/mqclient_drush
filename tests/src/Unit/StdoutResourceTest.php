<?php

namespace Drupal\Tests\mqclient_drush\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\mqclient_drush\Resource\StdoutResource;

/**
 * StdoutResource Tests.
 *
 * @coversDefaultClass \Drupal\mqclient_drush\Resource\StdoutResource
 */
class StdoutResourceTest extends UnitTestCase {

  /**
   * Test the dummy provider.
   *
   * @param string $message
   *   The message.
   *
   * @covers ::process
   * @dataProvider messageProvider
   */
  public function testProcess($message) {
    $resource = new StdoutResource();
    $result = $resource->process($message);
    $this->assertFalse($result, 'Always return FALSE');
  }

  /**
   * Data provider for testProcess.
   *
   * @return array
   *   provided data.
   */
  public function messageProvider(): array {
    return [
      ['Message'],
      [''],
    ];
  }

}
