<?php

namespace Drupal\mqclient_drush\Resource;

use Drupal\mqclient_drush\MqDrushResourceInterface;

/**
 * A dummy resource that outpouts some messages to stdout.
 */
class StdoutLadokResource implements MqDrushResourceInterface {

  /**
   * {@inheritdoc}
   */
  public function process(string $message): bool {
    $xml = trim(substr($message, strpos($message, '<?xml ')));
    // Do not show short messages to avoid risky tests.
    if (strlen($message) > 10) {
      echo "\n======BEGIN MESSAGE======\n";
      echo $xml;
      echo "\n======END MESSAGE======\n";
    }
    return self::RETURN_FOR_SAVE_FAILURE;
  }

}
