# MQClient Drush Commands #

This module provides Drush commands that can be used to read MQ data and pass it to other modules that process it.

The module was designed as an API module to be used in portals that require connections to IBM MQ.

### Installation ###

This module does not process or store any data, therefore, it is only useful when combined with other modules that provide it with the resources to process and persist incoming MQ messages. That is why this module should bu added to the composer.json file of another module or installation profile.

### Configuration ###

The following environment variables are used by the module to configure the MQ Client:
```
MQ_SERVER_ADDRESS
MQ_SERVER_PORT
MQ_SERVER_CHANNEL
MQ_SERVER_KEY_REPOSITORY
MQ_SERVER_AUTH_USER
MQ_SERVER_AUTH_PASS
MQ_SERVER_QUEUE_MANAGER
```
### Available Drush commands ###

```
drush mq:process_queue <queue name> <resource class> --presist=false --commit=false
```

```
drush mq:test_connection
```

```
drush mq:test_queue <queue_name>
```

### Who do I talk to? ###

* Al Saleh <alayham.saleh@gu.se>